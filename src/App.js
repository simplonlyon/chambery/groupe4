import React,{useState} from 'react';
import './App.css';
import Header from './Pages/Header';
import AddPerson from './Pages/AddPerson';
import Home from './Pages/Home';

function App() {
  const [page,setPage] = useState('Home');
  const handleChangePage = (nextPage) =>{
    setPage(nextPage)
  }  
  const displayPage = () => {
    if(page === 'Home') {
      return <Home />;
    }
    if(page === 'Add Person') {
      return <AddPerson />
    }

  }  
  return(
  <div>
    <Header nextPage = {(nextPage)=>handleChangePage(nextPage)}/>
    <div className="container-fluid">
          {displayPage()}
  
        </div>
    </div>
  );
}

export default App;
