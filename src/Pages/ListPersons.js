import React from 'react';

export default function ListPersons({users,selected,updateSelected}){
    return(
        <React.Fragment>    
            <h1>Persons List</h1>
                {JSON.stringify(selected)}
            <div className="list-group">
                {users.map(person => 
                    <button key={person.id} onClick={()=>updateSelected(person)} className="list-group-item">{person.name} {person.firstname}</button>
                )}
            </div>
        </React.Fragment>
    );
}