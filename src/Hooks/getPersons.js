import React, {useState, useEffect} from 'react';


export function useListPersons(){

    const [persons, setPersons] = useState([]);

    useEffect(async () => {
        const req = await fetch('http://localhost:4000/person')
        const result = await req.json();
        setPersons(result);
    }, []);

    return persons
}

